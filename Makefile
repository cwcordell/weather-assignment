image = wa:latest

docker-build:
	docker build -t $(image) .

docker-test:
	docker run --rm $(image) go test -v --cover ./...