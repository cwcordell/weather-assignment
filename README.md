## Build
To build the docker image run the following:
```
make docker-build
```
---

## Test
To test the application within the docker container run the following:
```
make docker-test
```