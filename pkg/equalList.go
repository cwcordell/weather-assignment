package pkg

// EqualList will return true if all of the items in one list is also in the other list and fals if not
func EqualList(a, b *Simple) bool {
	// if a, b, a.Head, or b.Head are nil then all of one is in the other
	if a == nil || b == nil || a.Head() == nil || b.Head() == nil {
		return true
	}

	// switch if b is larger
	if a.Size() < b.Size() {
		a,b = b,a
	}

	// for each b, loop thru a to find a match
	itemB := b.Head()
	itemA := a.Head()

	for {
		found := false
		// INNER:
		for {
			if itemB.Compare(itemA) {
				// Note: an optimization can be done here by moving found items to the beginning of the list
				// and using a pointer to maintain the not-found head but this would mutate the list order
				found = true
				break 
			}
			itemA = itemA.Next()
			if itemA == nil {
				break 
			}
		}

		// if not found then all of b is not in a
		if !found {
			return false
		}

		// get next b item
		itemB = itemB.Next()

		// if itemB is nil then the end of the list has been reached
		if itemB == nil {
			break
		}
	}
	// b's list has been exhausted and all have been found in a
	return true
}

// Item is a simple item list
type Item struct {
	NextItem *Item
	Index    int
}

// Next will return the next item in the list or nil if it is the last
func (o *Item) Next() *Item {
	if o.NextItem == nil {
		return nil
	}
	return o.NextItem
}

// Compare will return a bool indicating if the 
func (o Item) Compare(s *Item) bool {
	if s.Index != o.Index {
		return false
	}
	return true
}

// Simple is a generic implementation of the ISimple interface
type Simple struct {
	head  *Item
	tail  *Item
	count int
}

// Size will return the number of items in the list
func (o Simple) Size() int {
	return o.count
}

// Add will add an item to the end of the list
func (o *Simple) Add(s *Item) {
	if s == nil {
		return
	}
	// x, ok := s.(SingleItem)
	// if !ok {return}
	o.count++
	if o.head == nil {
		o.head = s
		o.tail = s
		return
	}
	o.tail.NextItem = s
	o.tail = o.tail.NextItem
}

// Head will return the first element in the list
func (o *Simple) Head() *Item {
	if o.head == nil {
		return nil
	}
	return o.head
}
