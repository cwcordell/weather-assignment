package pkg

import (
	"testing"
)
var (
	simpleNil Simple
	simpleEmpty = Simple{}
	simpleOne = Simple{}
	simpleThree = Simple{}
	simpleOdd = Simple{}
)

func init() {
	simpleOne.Add(&Item{Index:1})
	simpleThree.Add(&Item{Index:1})
	simpleThree.Add(&Item{Index:2})
	simpleThree.Add(&Item{Index:3})
	simpleOdd.Add(&Item{Index:1})
	simpleOdd.Add(&Item{Index:3})
	simpleOdd.Add(&Item{Index:5})
}

func TestEqualList(t *testing.T) {
	type args struct {
		a *Simple
		b *Simple
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "one-same", args: args{a: &simpleOne, b: &simpleOne}, want: true},
		{name: "three-same", args: args{a: &simpleThree, b: &simpleThree}, want: true},
		{name: "one-nil", args: args{a: &simpleNil, b: &simpleThree}, want: true},
		{name: "nil-empty", args: args{a: &simpleNil, b: &simpleEmpty}, want: true},
		{name: "double-nil", args: args{a: &simpleNil, b: &simpleNil}, want: true},
		{name: "one-empty", args: args{a: &simpleEmpty, b: &simpleThree}, want: true},
		{name: "double-empty", args: args{a: &simpleEmpty, b: &simpleEmpty}, want: true},
		{name: "one-three", args: args{a: &simpleOne, b: &simpleThree}, want: true},
		{name: "false", args: args{a: &simpleOdd, b: &simpleThree}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EqualList(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("EqualList() = %v, want %v", got, tt.want)
			}
		})
	}
}

// test data
var (
	item1 = &Item{Index: 1}
	one   = Simple{
		count: 1,
		head:  item1,
		tail:  item1,
	}

	item2 = &Item{Index: 2}
	two   = Simple{
		count: 2,
		head:  item1,
		tail:  item2,
	}
)

func init() {
	item1.NextItem = item2
}

func TestSimple_Add(t *testing.T) {
	type fields struct {
		head  *Item
		tail  *Item
		count int
	}
	type args struct {
		s *Item
	}
	tests := []struct {
		name          string
		simple        Simple
		args          args
		wantCount     int
		wantHeadIndex int
		wantTailIndex int
	}{
		{name: "empty", simple: Simple{}, args: args{s: &Item{Index: 1}}, wantCount: 1, wantHeadIndex: 1, wantTailIndex: 1},
		{name: "one", simple: one, args: args{s: &Item{Index: 2}}, wantCount: 2, wantHeadIndex: 1, wantTailIndex: 2},
		{name: "two", simple: two, args: args{s: &Item{Index: 3}}, wantCount: 3, wantHeadIndex: 1, wantTailIndex: 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.simple.Add(tt.args.s)
			if tt.simple.count != tt.wantCount {
				t.Errorf("Simple.Add() count = %v, want %v", tt.simple.count, tt.wantCount)
			}
			if tt.simple.head.Index != tt.wantHeadIndex {
				t.Errorf("Simple.Add() head index = %v, want %v", tt.simple.head.Index, tt.wantHeadIndex)
			}
			if tt.simple.tail.Index != tt.wantTailIndex {
				t.Errorf("Simple.Add() tail index = %v, want %v", tt.simple.tail.Index, tt.wantTailIndex)
			}
		})
	}
}

func TestSimple_Head(t *testing.T) {
	type fields struct {
		head  *Item
		tail  *Item
		count int
	}
	tests := []struct {
		name          string
		simple        Simple
		wantNoHead    bool
		wantCount     int
		wantHeadIndex int
		wantNextNil   bool
	}{
		{name: "empty", wantNoHead: true},
		{name: "one", simple: one, wantCount: 1, wantHeadIndex: 1, wantNextNil: false},
		{name: "two", simple: two, wantCount: 2, wantHeadIndex: 1, wantNextNil: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.simple.Head()
			if tt.wantNoHead {
				if got != nil {
					t.Error("Simple.Head() is not nil but wanted nil")
				}
				return
			}

			if tt.simple.count != tt.wantCount {
				t.Errorf("Simple.Head() count = %v, want %v", tt.simple.count, tt.wantCount)
			}
			if got.Index != tt.wantHeadIndex {
				t.Errorf("Simple.Head() head index = %v, want %v", tt.simple.head.Index, tt.wantHeadIndex)
			}
			if tt.wantNextNil != (got.NextItem == nil) {
				t.Errorf("Simple.Head() tail next = %v, want %v", tt.simple.tail.Index, tt.wantNextNil)
			}
		})
	}
}

func TestSingleItem_Compare(t *testing.T) {
	tests := []struct {
		name   string
		o *Item
		x *Item
		want   bool
	}{
		{name: "true", want: true, o: &Item{Index:1}, x: &Item{Index:1}},
		{name: "false", want: false, o: &Item{Index:1}, x: &Item{Index:2}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.o.Compare(tt.x); got != tt.want {
				t.Errorf("Item.Compare() = %v, want %v", got, tt.want)
			}
		})
	}
}