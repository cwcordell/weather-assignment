package pkg

import (
	"testing"
)

func setup(n int) *SingleItem {
	if n == 0 {
		return nil
	}
	head := &SingleItem{Index: n}
	current := head
	for {
		n--
		if n == 0 {
			break
		}
		current.NextItem = &SingleItem{Index: n}
		current = current.NextItem
	}
	return head
}

func TestFromEnd(t *testing.T) {
	tests := []struct {
		name       string
		setupItems *SingleItem
		target     int
		want       *SingleItem
	}{
		{name: "3-items-1-from-end", setupItems: setup(3), want: &SingleItem{Index: 1}, target: 1},
		{name: "8-items-5-from-end", setupItems: setup(8), want: &SingleItem{Index: 5}, target: 5},
		{name: "one", setupItems: setup(1), want: &SingleItem{Index: 1}, target: 1},
		{name: "short-of-target", setupItems: setup(5), target: 6, want: nil},
		{name: "target-zero", setupItems: setup(5), target: 0, want: nil},
		{name: "negative-target", setupItems: setup(5), target: -1, want: nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := FromEnd(tt.target, tt.setupItems)
			if tt.want == nil {
				if got != nil {
					t.Errorf("FromEnd() = wanted nil but got item")
					return
				}
			}  else if got == nil {
				t.Errorf("FromEnd(), got nil but want %v", tt.want.Index)
				return
			} else if tt.want.Index > 0 {
				if tt.want.Index != got.(*SingleItem).Index {
					t.Errorf("FromEnd() = %v, want %v", got, tt.want.Index)
					return
				}
			}
		})
	}
}

type testFifthFromEnd struct {
	res *SingleItem
}
func (o testFifthFromEnd) fromEnd(target int, l ISingle) ISingle {
	if o.res == nil {
		return nil
	}
	return o.res
}

func TestFifthFromEnd(t *testing.T) {
	tests := []struct {
		name       string
		tffe 		testFifthFromEnd
		want       *SingleItem
	}{
		{
			name: "success", 
			tffe: testFifthFromEnd {
				res: &SingleItem{Index:5},
			}, 
			want: &SingleItem{Index: 5},
		},
		{
			name: "nil-value", 
			tffe: testFifthFromEnd {
				res: nil,
				}, 
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			FromEnd = tt.tffe.fromEnd
			got := FifthFromEnd(&SingleItem{})
			if tt.want == nil {
				if got != nil {
					t.Errorf("FromEnd() = wanted nil but got item")
					return
				}
			} else {
				if tt.want.Index != got.(*SingleItem).Index {
					t.Errorf("FromEnd() = %v, want %v", got, tt.want.Index)
					return
				}
			}
		})
	}
}

func TestSimple_Size(t *testing.T) {
	tests := []struct {
		name   string
		simple Simple
		want   int
	}{
		{name: "empty", simple: Simple{}, want: 0},
		{name: "one", simple: Simple{count: 1}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.simple.Size(); got != tt.want {
				t.Errorf("Simple.Size() = %v, want %v", got, tt.want)
			}
		})
	}
}
