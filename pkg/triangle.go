package pkg

import (
	"errors"
	"strings"
)

// Instructions:
// Write a function that takes three sides of a triangle and answers if it's equilateral, isosceles, or scalene.

const (
	// Equilateral represent an equilateral triangle result
	Equilateral = "equilateral"
	// Isosceles represent an isosceles triangle result
	Isosceles = "isosceles"
	// Scalene represent an scalene triangle result
	Scalene = "scalene"
)

// TriangleType accepts three side diminsions of a triange and returns whether the triange is equilateral, isosceles, or scalene
// An error will be returned if the given diminsions are not valid
func TriangleType(a float32, b float32, c float32) (string, error) {
	// capture errors that result from validation
	var errs []string

	// validate side diminsions
	if a <= 0 {
		errs = append(errs, "Side a cannot be less than or equal to zero")
	}
	if b <= 0 {
		errs = append(errs, "Side b cannot be less than or equal to zero")
	}
	if c <= 0 {
		errs = append(errs, "Side c cannot be less than or equal to zero")
	}
	if len(errs) > 0 {
		return "", errors.New("Error:\n" + strings.Join(errs, "\n"))
	}

	// counter of equal sides
	eqCt := 0

	// count equal sides
	if a == b {
		eqCt++
	}
	if b == c {
		eqCt++
	}
	if a == c {
		eqCt++
	}

	// return result based on equal sides count
	switch eqCt {
	case 0:
		return Scalene, nil
	case 1:
		return Isosceles, nil
	default:
		return Equilateral, nil
	}
}
