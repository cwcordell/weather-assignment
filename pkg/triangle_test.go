package pkg

import "testing"

// TestTriangleType are unit tests for the TriangleType function
func TestTriangleType(t *testing.T) {
	type args struct {
		a float32
		b float32
		c float32
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "equilateral",
			args:    args{a: 4.4, b: 4.4, c: 4.4},
			want:    Equilateral,
			wantErr: false,
		},
		{
			name:    "isosceles-ab",
			args:    args{a: 4.4, b: 4.4, c: 6},
			want:    Isosceles,
			wantErr: false,
		},
		{
			name:    "isosceles-ac",
			args:    args{a: 2.1, b: 4.4, c: 2.1},
			want:    Isosceles,
			wantErr: false,
		},
		{
			name:    "isosceles-bc",
			args:    args{a: 2.1, b: 8.0, c: 8.0},
			want:    Isosceles,
			wantErr: false,
		},
		{
			name:    "scalene-abc",
			args:    args{a: 9.5, b: 8.0, c: 7.0},
			want:    Scalene,
			wantErr: false,
		},
		{
			name:    "scalene-acb",
			args:    args{a: 9.5, c: 8.0, b: 7.0},
			want:    Scalene,
			wantErr: false,
		},
		{
			name:    "scalene-bac",
			args:    args{b: 9.5, a: 8.0, c: 7.0},
			want:    Scalene,
			wantErr: false,
		},
		{
			name:    "scalene-bca",
			args:    args{b: 9.5, c: 8.0, a: 7.0},
			want:    Scalene,
			wantErr: false,
		},
		{
			name:    "scalene-cab",
			args:    args{c: 9.5, a: 8.0, b: 7.0},
			want:    Scalene,
			wantErr: false,
		},
		{
			name:    "scalene-cba",
			args:    args{c: 9.5, b: 8.1, a: 7.7},
			want:    Scalene,
			wantErr: false,
		},
		{
			name:    "error-a",
			args:    args{a: 0, b: 8.1, c: 7.7},
			want:    "",
			wantErr: true,
		},
		{
			name:    "error-b",
			args:    args{a: 0, b: 8.1, c: 7.7},
			want:    "",
			wantErr: true,
		},
		{
			name:    "error-c",
			args:    args{a: 0, b: 8.1, c: 7.7},
			want:    "",
			wantErr: true,
		},
		{
			name:    "error-ab",
			args:    args{a: 0, b: 0, c: 7.7},
			want:    "",
			wantErr: true,
		},
		{
			name:    "error-abc",
			args:    args{a: 0, b: -0.1, c: -7.7},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TriangleType(tt.args.a, tt.args.b, tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("TriangleType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("TriangleType() = %v, want %v", got, tt.want)
			}
		})
	}
}
