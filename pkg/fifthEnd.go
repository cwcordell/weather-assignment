package pkg

// FromEnd is a convenience variable for testing the clients of fromEnd
var FromEnd = fromEnd

// FifthFromEnd will return the 5th element from the end of a list or nil if the 5th element doesn't exist
func FifthFromEnd(l ISingle) ISingle {
	target := 5
	return FromEnd(target,l)
}

// fromEnd will return the nth element from the end of a list or nil if the nth element doesn't exist
func fromEnd(target int, l ISingle) ISingle {
	if l == nil || target == 0 {
		return nil
	}

	ct := 1
	head := l
	var tail ISingle

	for {
		// set tail if target has been reached or exceeded
		if ct == target {
			tail = l
		}

		// check if last element
		if head.Next() == nil {
			break
		}

		if tail != nil {
			tail = tail.Next()
		}

		// setup next round
		head = head.Next()
		ct++
	}
	return tail
}

// ISingle is an interface for a single linked list
type ISingle interface {
	Next() ISingle
}

// SingleItem is a generic implementation of the ISingle interface
type SingleItem struct {
	NextItem *SingleItem
	Index    int
}

// Next will return the next item in the list or nil if it is the last
func (o *SingleItem) Next() ISingle {
	if o.NextItem == nil {
		return nil
	}
	return o.NextItem
}
